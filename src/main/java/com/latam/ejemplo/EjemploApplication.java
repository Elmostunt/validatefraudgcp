package com.latam.ejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.latam")
@SpringBootApplication
public class EjemploApplication {
    public static void main(String[] args) {
        SpringApplication.run(EjemploApplication.class, args);
    }
}
