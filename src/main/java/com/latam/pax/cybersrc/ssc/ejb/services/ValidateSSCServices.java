package com.latam.pax.cybersrc.ssc.ejb.services;

import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;

public interface ValidateSSCServices {
    ValidateFraudRS processSSC(ValidateFraudRQ body) throws ClientWebServiceException;
}
