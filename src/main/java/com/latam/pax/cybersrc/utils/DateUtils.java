package com.latam.pax.cybersrc.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.latam.pax.cybersrc.domain.type.DateType;

public class DateUtils {
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

    public static final String DATE_FORMAT_LEGS = "yyyy-MM-dd HH:mm";

    /**
     * 
     * 
     * <p>
     * Register of versions:
     * <ul>
     * <li>1.0 12-07-2016, (Everis Chile) - initial release
     * </ul>
     * <p>
     * get date
     * 
     * @return
     * @since 1.0
     */
    public static String getDateServerString() {
        String dateFormatNow = "dd-MM-yyyy";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormatNow);
        return sdf.format(date);
    }

    /**
     * 
     * 
     * <p>
     * Register of versions:
     * <ul>
     * <li>1.0 11-07-2016, (Everis Chile) - initial release
     * </ul>
     * <p>
     * transforms the date format to the format that you specify
     * 
     * @param dateTimeString
     * @param format
     * @return
     * @since 1.0
     */
    public Calendar transformDateTime(String dateTimeString, String format) {
        SimpleDateFormat formatDate = new SimpleDateFormat(format);
        Calendar date = Calendar.getInstance();
        try {
            date.setTime(formatDate.parse(dateTimeString));
            return date;
        } catch (ParseException e) {
            logger.error("ParseException ", e);
            return null;
        }
    }

    /**
     * 
     * 
     * <p>
     * Register of versions:
     * <ul>
     * <li>1.0 11-07-2016, (Everis Chile) - initial release
     * </ul>
     * <p>
     * Method for the server date
     * 
     * @return
     * @since 1.0
     */
    public Calendar getDateServer() {
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        return date;
    }

    /**
     * 
     * 
     * <p>
     * Register of versions:
     * <ul>
     * <li>1.0 11-07-2016, (Everis Chile) - initial release
     * </ul>
     * <p>
     * Method for subtracting dates (year, day, hour, minute, secund)
     * 
     * @param dateInitial
     * @param dateEnd
     * @param dateType
     * @return
     * @since 1.0
     */
    public long getDiference(Calendar dateInitial, Calendar dateEnd, DateType dateType) {
        long dife = dateEnd.getTimeInMillis() - dateInitial.getTimeInMillis();
        return (dife / dateType.getFactor());
    }
}
