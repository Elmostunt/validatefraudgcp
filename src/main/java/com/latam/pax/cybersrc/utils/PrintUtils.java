package com.latam.pax.cybersrc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.latam.pax.cybersrc.commons.GlobalConstant;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class PrintUtils {
    @Autowired
    Environment env;
    @Getter
    @Setter
    Boolean isErrorEnable = "true".equalsIgnoreCase(env.getProperty(GlobalConstant.FLAG_ERROR_LOG)) ? Boolean.TRUE
            : Boolean.FALSE;

    private PrintUtils() {

    }

    /**
     * 
     * @param data
     */
    public static String printDebugData(String data) {

        log.debug(data);

        return data;

    }

    /**
     * 
     * @param data
     */
    public static String printInfoData(String data) {

        log.info(data);

        return data;
    }

    /**
     * 
     * @param data
     */
    public static String printWarnData(String data) {

        log.warn(data);
        return data;
    }

    /**
     * 
     * @param data
     */
    public static String printErrorData(String data) {

        log.error(data);
        return data;
    }

}
