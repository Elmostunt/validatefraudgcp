package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 */
@Data
public class ValidateFraudRQ implements Serializable {

    private static final long serialVersionUID = 2508261721217147158L;

//    @NotNull( message = "{domain.not.null}" )
    @Getter
    @Setter
    private SaleInformation saleInformation;
    @Getter
    @Setter
    private String merchantId;
    @Getter
    @Setter
    private Legs legs;

}
