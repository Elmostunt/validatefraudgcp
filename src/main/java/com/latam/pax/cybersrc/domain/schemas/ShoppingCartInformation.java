package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ShoppingCartInformation implements Serializable {

    private static final long serialVersionUID = -574752742716225951L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private Items items;

}
