package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <p>
 * Register of versions:
 * <ul>
 * <li>1.0 05-07-2016, (Everis Chile) - initial release
 * </ul>
 * <p>
 * This class contains the domain object
 * 
 * <p>
 * <B>All rights reserved by Lan.</B>
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class SalesOfficeInformation implements Serializable {

    private static final long serialVersionUID = 4849278242496581591L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private int salesAgentCode;
    @Getter
    @Setter
    private int stationNumber;
    @Getter
    @Setter
    private String countryISOCode;

}
