package com.latam.pax.cybersrc.impl;

import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.client.CyberSourceBRClient;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.br.utils.ParserRequestBRUtils;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.RejectMessage;
import com.latam.pax.cybersrc.domain.schemas.ServiceStatusType;
import com.latam.pax.cybersrc.domain.schemas.ShoppingCartInformation;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.domain.schemas.ValidationResult;
import com.latam.pax.cybersrc.utils.ParserCommonUtils;
import com.latam.pax.cybersrc.utils.PrintUtils;
import com.latam.pax.cybersrc.utils.XmlTools;
import com.latam.ws.ics.icstransaction.AFSService;
import com.latam.ws.ics.icstransaction.ObjectFactory;
import com.latam.ws.ics.icstransaction.ReplyMessage;
import com.latam.ws.ics.icstransaction.RequestMessage;

public final class ValidateBR {
    private static final Logger logger = LoggerFactory.getLogger(ValidateBR.class);

    private ValidateBR() {

    }

    public static ValidateFraudRS processBR(ValidateFraudRQ request, AppProperties prop)
            throws ClientWebServiceException {
        PrintUtils.printInfoData("Entre!" + XmlTools.toXml(request));
        ValidateFraudRS validateFraudRS = new ValidateFraudRS();
        ValidationResult validationResult = new ValidationResult();
        ServiceStatusType srvStatus = new ServiceStatusType();

        String issueType = request.getSaleInformation().getIsIssue() ? GlobalConstant.EMISSION
                : GlobalConstant.REMISSION;
        String isPayed = request.getSaleInformation().getIsPayed() ? "Y" : "N";
        String isDisruption = request.getSaleInformation().getIsDisruption() ? "Y" : "N";
        // G.Carcamo - Generacion de factory cybersouce para definir atributos de
        // llamada a servicio
        ObjectFactory factoryCybersource = new ObjectFactory();
        PrintUtils.printInfoData("ObjectFactory!");
        // Instancia de request dado jar ICS TRNASACTION, este como formato para llamada
        // a servicio
        RequestMessage requestCybersource = factoryCybersource.createRequestMessage();
        AFSService serviceAfs = factoryCybersource.createAFSService();
        requestCybersource.setAfsService(serviceAfs);
        serviceAfs.setRun(GlobalConstant.STR_TRUE);
        PrintUtils.printInfoData("AFSSERVICE!");
        // Obtencion de shoppingCart, para mapeo a rq destino nativo de numero de
        // canasta
        ShoppingCartInformation shoppingCartInformation = request.getSaleInformation().getShoppingCartInformation();
        requestCybersource.setMerchantReferenceCode(shoppingCartInformation.getItems().getItem().get(0).getPnrCode());
        // Instancia de response a mapear recibida respuesta desde servicio nativo
        PrintUtils.printInfoData("PNR!");
        ReplyMessage responseCyberSource;
        // G.Carcamo - Llenado de items, considerando nombre, apellido, fono de quien
        // genera cambio para reissue, entre otros
//      requestCybersource.setMerchantID(request.getMerchantId());
        if(prop.getMerchantIdBr().equals(request.getMerchantId())){
            requestCybersource.setMerchantID(prop.getMerchantIdBr());
        }
        ParserRequestBRUtils.fillItems(request, factoryCybersource, requestCybersource);
        // G.Carcamo - MDD's
        ParserRequestBRUtils.fillMddFields(request, issueType, isPayed, isDisruption, factoryCybersource,
                requestCybersource);
        PrintUtils.printInfoData("MDD" + XmlTools.toXml(requestCybersource));

        // G.Carcamo - llenado de decision manager
        ParserRequestBRUtils.fillDecisionManager(request, factoryCybersource, requestCybersource);

        try {
//G.Carcamo - Aqui hay que extraer el merchant id del RQ de origen, ademas
//enviarle variable Enviroment, esto en funcion de recuperar properties.
            CyberSourceBRClient client = new CyberSourceBRClient(request.getMerchantId(), prop);
            PrintUtils.printInfoData("RequestEntrada ->" + XmlTools.toXml(request));
            responseCyberSource = client.execute(requestCybersource);
            BigInteger reasonCode = responseCyberSource.getReasonCode();
            String country = request.getSaleInformation().getSalesOfficeInformation().getCountryISOCode();
            if (ParserCommonUtils.isApproved(reasonCode, prop)) {
                validationResult.setValidationRemark(ParserCommonUtils.getMessage(isDisruption, true, country));
                validationResult.setValid(true);
                validationResult.setRequestId(responseCyberSource.getRequestID());
                validateFraudRS.setValidationResult(validationResult);
                srvStatus.setCode(GlobalConstant.WS_SUCCESSFUL);
                srvStatus.setMessage(responseCyberSource.getReasonCode() + ":" + responseCyberSource.getDecision());
                validateFraudRS.setServiceStatus(srvStatus);
            } else {
                if (null != responseCyberSource.getReasonCode()) {
                    RejectMessage rejectMessage = new RejectMessage();
                    rejectMessage.setReasonCode(responseCyberSource.getReasonCode().longValue());
                    validationResult.setValidationRemark(ParserCommonUtils.getMessage(isDisruption, false, country));
                    validationResult.setValid(false);
                    validateFraudRS.setValidationResult(validationResult);
                    srvStatus.setCode(GlobalConstant.WS_SUCCESSFUL);
                    srvStatus.setMessage(responseCyberSource.getReasonCode() + ":" + responseCyberSource.getDecision());
                    validateFraudRS.setServiceStatus(srvStatus);
                }
            }
        } catch (ClientWebServiceException e) {
            logger.error(e.getMessage(), e);
        }

        return validateFraudRS;
    }

}
