package com.latam.pax.cybersrc.ejb.services;

import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;

public interface ValidateFraudServices {

    ValidateFraudRS execute(ValidateFraudRQ businessData, AppProperties prop) throws ClientWebServiceException;
}
