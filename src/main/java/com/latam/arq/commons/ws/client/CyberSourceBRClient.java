package com.latam.arq.commons.ws.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;

import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.client.handler.WSSUsernameTokenSecurityHandler;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.utils.PrintUtils;
import com.latam.ws.ics.icstransaction.ITransactionProcessor;
import com.latam.ws.ics.icstransaction.ReplyMessage;
import com.latam.ws.ics.icstransaction.RequestMessage;
import com.latam.ws.ics.icstransaction.TransactionProcessor;

import lombok.Getter;
import lombok.Setter;

public class CyberSourceBRClient {

    ITransactionProcessor port;
    @Getter
    @Setter
    private String userService;
    @Getter
    @Setter
    private String passService;

    public CyberSourceBRClient(String merchantId, AppProperties prop) throws ClientWebServiceException {
        try {
            URL urlWsdl = new URL(prop.getUrlTamCybersource());
            PrintUtils.printInfoData(urlWsdl.toString());

            setUserService(prop.getMerchantIdBr().equalsIgnoreCase(merchantId) ? prop.getSecTamBrUsername()
                    : prop.getSecTamIntUsername());
            setPassService(prop.getMerchantIdBr().equals(merchantId) ? prop.getTamBrCodeB64() : prop.getTamIntCodeB64());

            HandlerResolver handlerResolver = val -> {
                @SuppressWarnings("rawtypes")
                List<Handler> handlerListBr = new ArrayList<>();
                handlerListBr.add(new WSSUsernameTokenSecurityHandler(userService, passService));
                return handlerListBr;
            };
            QName serviceNameBr = new QName(GlobalConstant.WS_QNAME_ATTRIBUTE_ONE,
                    GlobalConstant.WS_QNAME_ATTRIBUTE_TWO);
            TransactionProcessor transactionProcessorBr = new TransactionProcessor(urlWsdl, serviceNameBr);
            transactionProcessorBr.setHandlerResolver(handlerResolver);

            port = transactionProcessorBr.getPortXML();

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                    prop.getEndpointTamCybersource());

        } catch (MalformedURLException e) {
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ClientWebServiceException(e.getMessage(), null);
        }
    }

    public ReplyMessage execute(RequestMessage requestMessage) throws ClientWebServiceException {
        try {
            return port.runTransaction(requestMessage);
        } catch (WebServiceException e) {
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ClientWebServiceException(e.getMessage(), null);
        }

    }
}
